package picket_test

import (
	"."
	"bytes"
	"io"
	"os"
	"regexp"
	"testing"
	"time"

	dgo "github.com/bwmarrin/discordgo"
)

var (
	now   = time.Now()
	since = now.Add(time.Hour * -72)
)

func TestSpool(t *testing.T) {
	token := os.Getenv("DC_UTOKEN")
	if token == "" {
		t.Skip("DC_UTOKEN not set")
	}
	chid := picket.Snowflake{}.WithID(260646654049386497)
	spl := picket.Spool{ChID: chid, Since: &since}
	s, err := dgo.New(token)
	if err != nil {
		t.Fatal(err)
	}
	mime := regexp.MustCompile("image/[a-b]+")
	msgs, errors := spl.ExtractURIs(s, 15, mime)
	go func() {
		for {
			err := <-errors
			if err != nil {
				t.Fatal(err)
			}
		}
	}()
	f, _ := os.OpenFile("test.html", os.O_CREATE|os.O_WRONLY, 0666)
	rd := io.TeeReader(bytes.NewReader([]byte(picket.HTM(msgs))), os.Stdout)
	io.Copy(f, rd)
	f.Close()
	// for msg := range msgs {
	// 	s, _ := picket.Snowflake{}.Parse(msg.ID)
	// 	fmt.Printf("%s\n", s.TimeStamp())
	// 	for uri := range picket.ExtractMsgURIs(msg) {
	// 		fmt.Println(uri)
	// 	}
	// }
}
