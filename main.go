package picket

import (
	"flag"
	"fmt"
	"net/smtp"
	"os"
	"regexp"
	"strings"
	"time"

	dgo "github.com/bwmarrin/discordgo"
	"github.com/jordan-wright/email"
)

func ck(err error) {
	if err != nil {
		panic(err)
	}
}

var (
	mimePattern *regexp.Regexp
	inbox       string
	chid        Snowflake
	since       time.Time
	s           *dgo.Session
)

const (
	sincePath = ".since"
	tsLayout  = time.RFC3339
)

func parseFlags() {
	var err error
	mime := flag.String("mime", "image/[a-z]", "Pull only URLs matching this content-type pattern")
	token := flag.String("token", "", "discord acct token")
	chidStr := flag.String("chid", "", "target channel ID")
	flag.StringVar(&inbox, "inbox", "", "target inbox")
	flag.Parse()
	if *token == "" {
		fmt.Fprintf(os.Stderr, "fatal: please provide a token\n")
		os.Exit(1)
	}
	if *chidStr == "" {
		fmt.Fprintf(os.Stderr, "fatal: please provide a target channel ID\n")
		os.Exit(2)
	}
	if inbox == "" {
		fmt.Fprintf(os.Stderr, "fatal: please provide a recipient inbox\n")
		os.Exit(3)
	}
	s, err = dgo.New(*token)
	ck(err)
	s.StateEnabled = true
	s.State.MaxMessageCount = 100

	if *mime != "" {
		mimePattern = regexp.MustCompile(*mime)
	}
	if stat, err := os.Stat(sincePath); err == nil {
		since = stat.ModTime()
		thePresent := time.Now()
		os.Chtimes(sincePath, thePresent, thePresent)
	} else {
		fp, _ := os.OpenFile(sincePath, os.O_CREATE, 0400)
		fp.Close()
	}
}

func HTM(uris <-chan string) string {
	var payload strings.Builder
	payload.WriteString(htmHeader)
	for uri := range uris {
		payload.WriteString(fmt.Sprintf("<img src=%s/>\n", uri))
		fmt.Println(uri)
	}
	payload.WriteString(htmFooter)
	return payload.String()
}

func chname() string {
	ch, err := s.Channel(chid.String())
	ck(err)
	switch ch.Type {
	case dgo.ChannelTypeGroupDM, dgo.ChannelTypeDM:
		unames := make([]string, 0, len(ch.Recipients))
		for _, u := range ch.Recipients {
			unames = append(unames, fmt.Sprintf("@%s", u.String()))
		}
		return strings.Join(unames, ", ")
	default:
		g, err := s.Guild(ch.GuildID)
		ck(err)
		return fmt.Sprintf("%s#%s", g.Name, ch.Name)
	}
}

func main() {
	parseFlags()
	spl := Spool{
		Since: &since,
		ChID:  chid}
	uris, errors := spl.ExtractURIs(s, 15, mimePattern)
	go func() {
		ck(<-errors)
	}()
	chName := make(chan string)
	go func() {
		defer close(chName)
		chName <- chname()
	}()
	email := email.NewEmail()
	email.To = []string{inbox}
	email.From = "iseurie@gmail.com"
	email.Subject = fmt.Sprintf("%s: URI dump [%s]", <-chName, mimePattern.String())
	rd := strings.NewReader(HTM(uris))
	_, err := email.Attach(rd, "links.html", "text/html")
	ck(err)
	auth := smtp.PlainAuth("", "iseurie", "make me fade", "gmail.com")
	err = email.SendWithTLS("smtp.gmail.com:587", auth, nil)
	ck(err)
}
