package picket_test

import (
	"."
	"testing"
	"time"
)

func TestSnowflake(t *testing.T) {
	s := picket.Snowflake{}.WithID(488731373637730334)
	read := s.TimeStamp()
	canon := time.Unix(1536593039, 0)
	if read.Unix() != canon.Unix() {
		t.Fatalf("Snowflake: timestamp read failure; %s off", canon.Sub(read))
	}
}
