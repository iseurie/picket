package picket

import (
	// "fmt"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	dgo "github.com/bwmarrin/discordgo"
	"gopkg.in/jdkato/prose.v2"
)

type Spool struct {
	Before, Since *time.Time
	ChID          Snowflake
}

func (spl Spool) snowflakeBounds() (before, after string) {
	if spl.Since != nil {
		after = Snowflake{}.WithTS(*spl.Since).String()
	}
	if spl.Before != nil {
		before = Snowflake{}.WithTS(*spl.Before).String()
	}
	return
}

func isURI(raw string) bool {
	_, err := url.ParseRequestURI(raw)
	return err == nil
}

func ExtractMsgURIs(msg *dgo.Message) <-chan string {
	sink := make(chan string)
	var src strings.Builder
	src.WriteString(msg.ContentWithMentionsReplaced())
	for _, embed := range msg.Embeds {
		src.WriteString("\n\n")
		src.WriteString(embed.Description)
		if embed.Video != nil {
			sink <- embed.Video.URL
		}
		if embed.Image != nil {
			sink <- embed.Image.URL
		}
	}
	doc, err := prose.NewDocument(
		src.String(),
		prose.WithExtraction(false),
		prose.WithSegmentation(false),
		prose.WithTokenization(true),
		prose.WithTagging(false))
	ck(err)
	var extracted sync.WaitGroup
	extracted.Add(2)
	go func() {
		defer extracted.Done()
		for _, t := range doc.Tokens() {
			if isURI(t.Text) {
				sink <- t.Text
			}
		}
	}()

	go func() {
		defer extracted.Done()
		for _, a := range msg.Attachments {
			sink <- a.URL
		}
	}()

	go func() {
		defer close(sink)
		extracted.Wait()
	}()
	return sink
}

func (spl Spool) Unroll(s *dgo.Session, chunkSz int) (<-chan *dgo.Message, chan error) {
	before, after := spl.snowflakeBounds()
	errors := make(chan error)
	if chunkSz <= 1 {
		chunkSz = 2
	}
	sink := make(chan *dgo.Message, chunkSz)
	go func() {
		defer close(sink)
		defer close(errors)
		for {
			M, err := s.ChannelMessages(spl.ChID.String(), chunkSz, after, before, "")
			if err != nil {
				errors <- err
				return
			}
			if len(M) == 0 {
				break
			}
			for m := range M[:len(M)-1] {
				sink <- M[m]
				if spl.Since != nil {
					s, _ := Snowflake{}.Parse(M[m].ID)
					t := s.TimeStamp()
					// check if this timestamp comes before our anchor
					if t.Sub(*spl.Since) < 0 {
						return
					}
				}
			}
			after = M[len(M)-1].ID
		}
	}()
	return sink, errors
}

func (spl Spool) ExtractURIs(s *dgo.Session, chunkSz int, contentMime *regexp.Regexp) (<-chan string, <-chan error) {
	sink := make(chan string, chunkSz)
	msgs, errors := spl.Unroll(s, chunkSz)
	go func() {
		defer close(sink)
		var extracted sync.WaitGroup
		for msg := range msgs {
			extracted.Add(1)
			go func(msg *dgo.Message) {
				defer extracted.Done()
				uris := ExtractMsgURIs(msg)
				for uri := range uris {
					if contentMime != nil {
						head, err := http.Head(uri)
						if err != nil {
							continue
						}
						tp := []byte(head.Header.Get("Content-Type"))
						if !contentMime.Match(tp) {
							continue
						}
					}
					sink <- uri
				}
			}(msg)
		}
		extracted.Wait()
	}()
	return sink, errors
}
