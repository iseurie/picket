package picket

import (
	"strconv"
	"time"
)

var discordEpoch = time.Unix(1420070400, 0)

type Snowflake struct {
	uint64
	me bool
}

func (s *Snowflake) TimeStamp() time.Time {
	elapsed := time.Duration(s.uint64>>22) * time.Millisecond
	return discordEpoch.Add(elapsed)
}

func (s Snowflake) WithTS(t time.Time) Snowflake {
	elapsed := t.Sub(discordEpoch)
	s.uint64 |= uint64(elapsed) << 22
	return s
}

func (s Snowflake) Parse(x string) (Snowflake, error) {
	u, err := strconv.ParseUint(x, 10, 64)
	s.uint64 = u
	return s, err
}

func (s Snowflake) String() string {
	if s.me {
		return "@me"
	}
	return strconv.FormatUint(s.uint64, 10)
}

func (s Snowflake) WithID(i uint64) Snowflake {
	s.uint64 = i
	s.me = false
	return s
}

func (s Snowflake) ID() uint64 {
	return s.uint64
}
