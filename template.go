package picket

const htmHeader = `
<!doctype html>
<html>
<head>
<style>
div {
	display: flex;
}
</style>
</head>
<body>
<div>
`

const htmFooter = `
</div>
</body>
</html>
`

type htmNode struct {
	Body, Atom string
	Parent     *htmNode
	Children   []*htmNode
}
